#include "codecreator.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CodeCreator w;
    w.show();

    return a.exec();
}
