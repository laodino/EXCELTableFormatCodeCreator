﻿#ifndef CODECREATOR_H
#define CODECREATOR_H

#include <QMainWindow>
#include <QXmlStreamReader>
#include <QFile>
#include <QFileDialog>
#include <QDebug>
#include <QDateTime>
#include <io.h>
#include <QFile>
#include <QAxObject>

namespace Ui {
class CodeCreator;
}

class CodeCreator : public QMainWindow
{
    Q_OBJECT

public:
    explicit CodeCreator(QWidget *parent = 0);
    ~CodeCreator();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::CodeCreator *ui;

    void readxmlfile();


    QString CFN(int col,int row);

    void WriteCode(QString cell, QString text, bool ismerge);

    void mergecell(QString cell);

    void exportToExcel();

    void ParseXML(QXmlStreamReader *reader);

};

#endif // CODECREATOR_H
