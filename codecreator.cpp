﻿#include "codecreator.h"
#include "ui_codecreator.h"
#pragma execution_character_set("utf-8")

CodeCreator::CodeCreator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CodeCreator)
{
    ui->setupUi(this);
}

CodeCreator::~CodeCreator()
{
    delete ui;
}


void CodeCreator::readxmlfile()
{
    //将xml文件读取数据
    QString file_name = QFileDialog::getOpenFileName(NULL,"打开文件",".","*.xml");
    QFile file(file_name);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))

    {
        //构建QXmlStreamReader对象
        QXmlStreamReader reader(&file);
        //ParseEntry(&reader);
        ParseXML(&reader);
        file.close();
    }
    else
    {
        qDebug()<<"Open file xml failure";

    }

}




void CodeCreator::on_pushButton_clicked()
{
    ui->textBrowser->clear();
    readxmlfile();

}

//数字转EXCEL列字母
QString CodeCreator::CFN(int col,int row)
{
    QString resultStr = "";
    while(col > 0)
    {
        int k = col % 26;
        if(k == 0)
            k = 26;
        resultStr.push_front(QChar(k + 64));
        col = (col - k) / 26;
    }

    return resultStr+QString::number(row);
}

//格式和文本写入
void CodeCreator::WriteCode(QString cell, QString text,bool ismerge)
{
    if(!ismerge)
    {
        ui->textBrowser->append("mr = worksheet->querySubObject(\"Range(const QString&)\",\""+cell+"\");");
    }

    // ui->textBrowser->append("mr->setProperty(\"HorizontalAlignment\", -4108);");
    //ui->textBrowser->append("mr->setProperty(\"VerticalAlignment\", -4108);");
    //ui->textBrowser->append("mr->setProperty(\"WrapText\", true);");
    ui->textBrowser->append("mr->dynamicCall(\"SetValue(const QVariant&)\",\""+text+"\");");
    ui->textBrowser->append("                                                             ");
}

//合并单元格
void CodeCreator::mergecell(QString cell)
{
    ui->textBrowser->append("mr = worksheet->querySubObject(\"Range(const QString&)\",\""+cell+"\");");
    ui->textBrowser->append("mr->setProperty(\"MergeCells\", true);");
    ui->textBrowser->append("                                                             ");
}


void CodeCreator::exportToExcel()
{
    QString fileName = QFileDialog::getSaveFileName(NULL, QStringLiteral("保存文件"),  "", "EXCEL(*.xls)");
    if(fileName == ""){
        return;
    }
    fileName = QDir::toNativeSeparators(fileName);

    int row =8;
    int col = 21;

    if(row == 0 || col == 0){
        return;
    }
    if(row > 0){

        // step1：连接控件

        QAxObject* excel = new QAxObject();
        excel->setControl("Excel.Application");  // 连接Excel控件
        excel->dynamicCall("SetVisible (bool Visible)", "false"); // 不显示窗体
        excel->setProperty("DisplayAlerts", false);  // 不显示任何警告信息。如果为true, 那么关闭时会出现类似"文件已修改，是否保存"的提示

        // step2: 打开工作簿
        QAxObject* workbooks = excel->querySubObject("WorkBooks"); // 获取工作簿集合
        //焊接
        // 打开工作簿方式一：新建
        workbooks->dynamicCall("Add"); // 新建一个工作簿

        QAxObject* workbook = excel->querySubObject("ActiveWorkBook"); // 获取当前工作簿
        QAxObject *work_sheets = workbook->querySubObject("Sheets");
        int sheet_count = work_sheets->property("Count").toInt();
        // step3: 打开sheet
        QAxObject* worksheet = work_sheets->querySubObject("Item(int)", sheet_count); // 获取工作表集合的工作表1， 即sheet1
        worksheet->setProperty("Name", "test");
        QAxObject *mr;
        qDebug()<<"start";



        qDebug()<<"end";
        //----------------------------------------------------------------------

        workbook->dynamicCall("SaveAs (const QString&, int, const QString&, const QString&, bool, bool)",
                              fileName, 56, QString(""), QString(""), false, false);

        workbook->dynamicCall("Close(Boolean)", false);  //关闭文件
        delete excel;//一定要记得删除，要不线程中会一直打开excel.exe
    }


}


void CodeCreator::on_pushButton_2_clicked()
{
    exportToExcel();
}

void CodeCreator::ParseXML(QXmlStreamReader *reader)
{
    int row=0,col= 0;
    bool ismerge = false;
    int mergecellstartrow=0,mergecellstartcol=0,mergecellendrow=0,mergecellendcol = 0;

    // QXmlStreamReader::TokenType type;
    while(!reader->atEnd())
    {
        if(reader->isStartElement())
        {
            qDebug()<<reader->name();
            if(reader->name()=="Row")
            {
                row +=1;
                col = 0;
            }
            else if (reader->name()=="Cell")
            {
                QXmlStreamAttributes  attributes =reader->attributes();
                if(attributes.hasAttribute("ss:Index"))
                {
                    col = attributes.value("ss:Index").toInt();
                }
                else
                {
                    col+=1;
                }

                mergecellstartrow =row;
                mergecellstartcol =col;
                mergecellendrow= row;
                mergecellendcol = col;
                if(attributes.hasAttribute("ss:MergeAcross"))
                {
                    ismerge = true;
                    mergecellendcol = col+attributes.value("ss:MergeAcross").toInt();
                    col+=attributes.value("ss:MergeAcross").toInt();
                }
                if(attributes.hasAttribute("ss:MergeDown"))
                {
                    ismerge = true;
                    mergecellendrow = row+attributes.value("ss:MergeDown").toInt();
                }
                if(ismerge)
                {
                    //mergecell(mergestartcell,mergcellend)
                    mergecell(CFN(mergecellstartcol,mergecellstartrow)+":"+CFN(mergecellendcol,mergecellendrow));
                }
            }
            else if(reader->name()=="Data")
            {
                QString cellposition="";
                cellposition=ismerge?CFN(mergecellstartcol,mergecellstartrow)+":"+CFN(mergecellendcol,mergecellendrow):CFN(col,row)+":"+CFN(col,row);
                if(ismerge)
                {
                    cellposition=CFN(mergecellstartcol,mergecellstartrow)+":"+CFN(mergecellendcol,mergecellendrow);
                    WriteCode(cellposition,reader->readElementText(),true);
                }
                else
                {
                    cellposition=CFN(col,row)+":"+CFN(col,row);
                    WriteCode(cellposition,reader->readElementText(),false);
                }
                qDebug()<<reader->readElementText();

            }
            else
            {
                //continue;
            }
        }
        else if(reader->isEndElement())
        {
            qDebug()<<reader->name();
            if(reader->name()=="Row")
            {
                //  row+=1;
            }
            else if (reader->name()=="Cell")
            {
                ismerge = false;
                mergecellstartrow=0,mergecellstartcol=0,mergecellendrow=0,mergecellendcol = 0;
            }
            else if(reader->name()=="Data")
            {

            }
            else
            {

            }
        }
        else
        {
            //  qDebug()<<"other";
        }

        reader->readNext();
    }
}

